stages:
  - build
  - test
  - check_results
  - deploy
  - dast

variables:
  DOCKER_IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  vulnerable_tag: vulnerable
  non_vulnerable_tag: non_vulnerable



# Build Docker image
# -----------------------------

build:
  stage: build
  image: 
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$vulnerable_tag


# Scan Docker container for vulnerabilities
# -----------------------------------------

# Please note that this will only show vulnerabilities during build time.
# Image needs to be rebuilt regularly to detect new vulverabilities.
# upstream doc: https://docs.gitlab.com/ee/user/application_security/container_scanning/
include:
  template: Container-Scanning.gitlab-ci.yml
# the container_scanning job template needs to be extended to work in CERN environment
container_vulnerability_scanning:
  extends: container_scanning
  stage: test
  tags:
  # privileged runners need to be explicitly requested
  #- docker-privileged
  variables:
    GIT_STRATEGY: fetch
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE
    # needs to match the tag built in the build stage
    CI_APPLICATION_TAG: $vulnerable_tag
  # run the scanning job even if GitLab doesn't support analysis of scan result
  rules:
    - if: '$CI =~ /.*/'
      when: always
  # Required to pass the results to the result stage
  artifacts:
    paths:
      - gl-container-scanning-report.json


# Check scanning results
# ----------------------

# Check if the previous scanning reported any vulnerability
scanning_results:
  stage: check_results
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
  - echo "script vol"
  - cat ./gl-container-scanning-report.json  
  # Use a simple jq command to check vulnerabilities in the previous JSON report
  - jq -e "( .vulnerabilities | length ) == 0" ./gl-container-scanning-report.json
  dependencies:
    - container_vulnerability_scanning
  # Comment if you want job to ignore if there are vulnerabilities
  allow_failure: false



deploy:
  stage: deploy
  variables:
    APP_NAME: production-todo
    APP_LABEL: production
    DEPLOY_HOST: todo.k8s.fire.org
  environment:
    name: production
    url: http://todo.k8s.fire.org:32445/
  image: roffe/kubectl:v1.13.0
  script:
    - kubectl delete --ignore-not-found=true secret gitlab-auth
    - kubectl create secret docker-registry gitlab-auth --docker-server=$CI_REGISTRY --docker-username=$KUBE_PULL_USER --docker-password=$KUBE_PULL_PASS
    - cat k8s.yaml | envsubst | kubectl apply -f -
  only:
    - master

dast:
  stage: dast
  image: registry.gitlab.com/gitlab-org/security-products/zaproxy
  variables:
    website: "http://todo.k8s.fire.org:32408/"
  allow_failure: true
  script:
    - mkdir /zap/wrk/
    - /zap/zap-baseline.py -t $website -g gen.conf -r testreport.html || true
    - cp /zap/wrk/testreport.html .
  artifacts:
    reports:
      dast: testreport.html
  only:
    - master

dast-review:
  stage: dast
  image: registry.gitlab.com/gitlab-org/security-products/zaproxy
  variables:
    website: http://${CI_COMMIT_REF_SLUG}-todo.k8s.fire.org:32445/
  allow_failure: true
  script:
    - mkdir /zap/wrk/
    - /zap/zap-baseline.py -J gl-dast-report.json -t $website || true
    - cp /zap/wrk/gl-dast-report.json .
  artifacts:
    reports:
      dast: gl-dast-report.json
  except:
    - master
